#
# Makefile
# $Id: Makefile 1439 2013-07-31 12:42:05Z sgolovan $
#

%.txt: %.sgml
	LANG=C debiandoc2text $<

%.html/index.html: %.sgml
	LANG=C debiandoc2html $<

%.pdf: %.sgml
	LANG=C debiandoc2pdf $<

all: txt pdf html
txt text: tcltk-policy.txt
pdf: tcltk-policy.pdf
html: tcltk-policy.html/index.html

clean:
	-rm -f tcltk-policy.txt
	-rm -f tcltk-policy.tpt
	-rm -f tcltk-policy.pdf
	-rm -rf tcltk-policy.html

.PHONY: txt text pdf html clean all
